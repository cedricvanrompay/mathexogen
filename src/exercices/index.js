import algebra from './algebra';
import functions from './functions';
import trigonometry from './trigonometry';

export {
  algebra,
  functions,
  trigonometry,
}