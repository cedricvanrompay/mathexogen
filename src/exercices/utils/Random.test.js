import Random from './Random';

describe('integer generation', () => {
  test('normal', () => {
    const random = new Random('1234');
    
    const expected = [3, 4, 9, 9, 3, 0];
    const generated = Array.from(
      new Array(6),
      (x) => random.integer({ min: 0, max: 10 })
    )

    expect(generated).toEqual(expected);
  })

  test('with excluded values', () => {
    const random = new Random('1234');

    const expected = [4, 6, 11, 11, 4, 0]
    const generated = Array.from(
      new Array(6),
      (x) => random.integer({ min: 0, max: 12, exclude: [2, 5] })
    )

    expect(generated).toEqual(expected);
  })

  test('with duplicate and unordered excluded values', () => {
    const random = new Random('1234');

    const expected = [4, 6, 11, 11, 4, 0]
    const generated = Array.from(
      new Array(6),
      (x) => random.integer({ min: 0, max: 12, exclude: [5, 2, 2] })
    )

    expect(generated).toEqual(expected);
  })

  test('excluded values are honored', () => {
    const random = new Random('1234');

    const generated = Array.from(
      new Array(20),
      () => random.integer({ min: 3, max: 13, exclude: [9, 8, 10] })
    )

    expect(generated.includes(8)).toBe(false)
    expect(generated.includes(9)).toBe(false)
    expect(generated.includes(10)).toBe(false)
  })
})

describe('choice', () => {
  const random = new Random('1234');

  const options = ['A', 'B', 'C', 'D', 'E', 'F'];
  expect(random.choice(options)).toEqual('B');
})