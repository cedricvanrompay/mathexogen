import graphReading from './graphReading';
import linearEquation from './linearEquation';

graphReading.title = 'Lecture sur un graphe';
linearEquation.title = "Équation d'une droite";

export default {
  graphReading,
  linearEquation,
}