import listFunctionsPathDeep from './listFunctionsPathDeep';

describe('mapFunctionsDeep', () => {
  it('works on expected structure', () => {
    const input = {
      'foo': () => null,
      'bar': {
        'lorem': () => null,
        'ipsum': () => null,
      }
    }

    const result = listFunctionsPathDeep(input);

    expect(Object.keys(result)).toEqual([
      'foo',
      'bar.lorem',
      'bar.ipsum',
    ])
  })
})